<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Thujohn\Twitter\Facades\Twitter as Twitter;

class tweets extends Command{
    private $howManyTweets = 50;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:tweets {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import last 50 tweets from user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = $this->argument('user');
        $tweets = Twitter::getUserTimeline(['screen_name' => $user, 'count' => $this->howManyTweets, 'tweet_mode' => 'extended', 'format' => 'array']);

        foreach($tweets as $tweet){
            echo("\n-----");
            echo("\nTweet: "); print_r($tweet['full_text']);
            echo("\nLikes: "); print_r($tweet['favorite_count']);
            echo("\nRetweets: "); print_r($tweet['retweet_count']);
            echo("\nReplies to Tweet: "); print_r($tweet['in_reply_to_screen_name']);
            echo("\n\n");
        }
    }
}
