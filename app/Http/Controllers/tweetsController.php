<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Thujohn\Twitter\Facades\Twitter as Twitter;

use App\Tweet;

class tweetsController extends Controller{

    private $filterURLParam;
    private $howManyTweets = 50;

    public function getTweets($user, Request $request) {

        //Verifico se a URL foi montada corretamente, e se não for, retorno a mensagem de erro para a View
        if( !($this->isURLValid($request)) ){
            return view('tweets', [
            'msg' => "Quando utilizados, os parâmetros ORDER e ORDER_BY precisam ser ambos setados corretamente :("
            ]);
        }


        //Pego os Tweets
        $tweets = collect(Twitter::getUserTimeline(['screen_name' => $user, 'count' => $this->howManyTweets, 'tweet_mode' => 'extended', 'format' => 'array']));


        //Salvo Tweets no MongoDB
        $this->saveTweetsOnMongoDB($tweets);


        //Verifico se existe algum tipo de organização nos tweets, e caso sim, aplico o ordenamento apropriado
        if ($this->areTweetsOrganized($request)){
            $this->setFilter($request);
            $tweets = $this->applyOrganization($request, $tweets);
        }


        /*Paginação */
        $currentPage = LengthAwarePaginator::resolveCurrentPage();// Pega a página atual da url
        $perPage = 3;// Defino quantos tweets por página
        $currentPageItems = $tweets->slice(($currentPage * $perPage) - $perPage, $perPage)->all(); // Divido a coleção de tweets pra mostrar os tweets da página atual
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($tweets), $perPage); // Cria a paginação para passar pra view
        $paginatedItems->setPath($request->url()); // Seta a url para o paginador


        //Retorno os tweets tratados para a View
        return view('tweets', [
            'name' => $tweets[0]['user']['name'],
            'tweets' => $paginatedItems->appends($request->except('page')),
            'msg' => ''
        ]);

    }




    public function isURLValid($request){
        //URL válida: Se ela possui parâmetro de ordenamento E de filtro, OU se ela não possui parametro de ordenamento NEM de filtro
        if( ($request->has('order') && $request->has('order_by')) || (!$request->has('order') && !$request->has('order_by')) ){
            return true;
        }
        else{
            return false;
        }

    }

    public function areTweetsOrganized($request){
        if( ($request->has('order')) && ($request->has('order_by')) ){
            return true;
        }
        else{
            return false;
        }
    }

    public function setFilter($request){
        //Opções que podem ser usadas para filtro
        switch($request->input('order_by')){
            case 'likes':
                $this->filterURLParam = 'favorite_count';
                break;

            case 'retweets':
                $this->filterURLParam = 'retweet_count';
                break;

            default:
                $this->filterURLParam = 'oiiii';
                break;
        }
    }

    public function applyOrganization($request, $tweets){
        //Ordeno os tweets de acordo
        if($request->has('order') && ($request->input('order') == 'ASC')){
            return $tweets->sortBy($this->filterURLParam, 0, false);
        }
        else if($request->has('order') && ($request->input('order') == 'DESC')) {
            return $tweets->sortBy($this->filterURLParam, 0, true);
        }
    }

    public function saveTweetsOnMongoDB($tweets){
      foreach($tweets as $tweet) {
        $tweetDocument = new Tweet();
        $tweetDocument->setCollection($tweet['user']['screen_name']);
        $tweetDocument->name = $tweet['user']['name'];
        $tweetDocument->full_text =  $tweet['full_text'];
        $tweetDocument->favorite_count = $tweet['favorite_count'];
        $tweetDocument->retweet_count = $tweet['retweet_count'];
        $tweetDocument->save();
      }
    }
}
