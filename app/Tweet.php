<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Tweet extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'tweets';

    protected $fillable = [
        'name', 'full_text', 'favorite_count', 'retweet_count'
    ];

    public function setCollection($col){
      $this->collection = $col;
    }
}
