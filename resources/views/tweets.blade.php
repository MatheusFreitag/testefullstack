<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link rel="stylesheet" href="{{ url('/css/app.css') }}">
        <title>Twitterfy</title>
    </head>
    <body>
        @if($msg == '')
            <h2>Tweets de {{$name}} </h2>

            @foreach($tweets as $tweet)
                <div class="row">
                    <div class="col s12 m10 offset-m1">
                      <div class="card blue darken-1">
                        <div class="card-content white-text">
                         <span class="card-title"><img src="{{$tweet['user']['profile_image_url_https']}}"></img></span>
                          <p>{{$tweet['full_text']}}</p>
                        </div>
                        <div class="card-action">
                          <span>Likes: {{$tweet['favorite_count']}}</span>
                          <span class="right">Retweets: {{$tweet['retweet_count']}}</span>
                        </div>
                      </div>
                    </div>
                </div>
            @endforeach

            <div>
                {{ $tweets->links() }}
            </div>
        @else

            <h1>{{$msg}}</h1>

        @endif

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
</html>
